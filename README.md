## Variables

Most of these need to be pre-defined before import

- `ARDUINO_DIR` (default: `/usr/share/arduino`)
- `FQBN` (or `ARDMK_VENDOR`, `ARCHITECTURE`, & `BOARD_TAG`)
- `BOARD_SUB`
- `PROGRAMMER` (only needed if uploading)
- `PROJECT_NAME` (defaults: cwd name)
- `{C,CPP,INO,S}_SRCS` to explicitly define source files
- `BUILD_DIR` (default: `./build`)

## Usage

Example:
```
FQBN=arduino:avr:mega
PROGRAMMER=avrispmkii
include Arduino.mk
serial.port=$(wildcard /dev/ttyACM*)
```

## Benchmarks

### From scratch

+---------------------+-----------+-----------+-------------------------+----------------------------+
| arduino-cli compile | make -j 1 | make -j   | (arduino-makefile) make | (arduino-makefile) make -j |
+=====================+===========+===========+=========================+============================+
| 2m12.318s           | 1m44.218s | 0m30.672s | 1m57.082s               | 0m52.343s                  |
+---------------------+-----------+-----------+-------------------------+----------------------------+

### Incremental build

```
echo > ./src/HAL/AVR/inc/SanityCheck.h
```

+---------------------+-----------+-----------+-------------------------+----------------------------+
| arduino-cli compile | make -j 1 | make -j   | (arduino-makefile) make | (arduino-makefile) make -j |
+=====================+===========+===========+=========================+============================+
| 1m13.025s           | 0m56.302s | 0m16.957s | 0m59.670s               | 0m23.850s                  |
+---------------------+-----------+-----------+-------------------------+----------------------------+
