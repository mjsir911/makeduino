MAKEFLAGS += --no-builtin-rules

ARDUINO_DIR?=/usr/share/arduino

ifdef FQBN
ARDMK_VENDOR?=$(word 1,$(subst :, ,$(FQBN)))
ARCHITECTURE?=$(word 2, $(subst :, ,$(FQBN)))
BOARD_TAG?=$(word 3,$(subst :, ,$(FQBN)))
endif

runtime.os := linux
load_spec = $(eval $(subst .${runtime.os},, $(subst {,$${, $(file < ${1}))))
build.arch := $(shell tr '[:lower:]' '[:upper:]' <<< "${ARCHITECTURE}")
runtime.hardware.path ?= $(ARDUINO_DIR)/hardware
runtime.platform.path ?= ${runtime.hardware.path}/$(ARDMK_VENDOR)/${ARCHITECTURE}
# runtime.ide.version := $(file < ${ARDUINO_DIR}/lib/version.txt)
runtime.ide.version := 0
PROJECT_NAME ?= $(shell basename $$(realpath .))
build.project_name = $(PROJECT_NAME)

$(call load_spec,${runtime.hardware.path}/platform.txt)
$(call load_spec,${runtime.platform.path}/platform.txt)
$(call load_spec,$(runtime.platform.path)/boards.txt)
$(call load_spec,$(runtime.platform.path)/programmers.txt)

compiler.cpp.extra_flags:=$(CPPFLAGS)
compiler.c.extra_flags:=$(CFLAGS)

use_namespace = $(foreach varname,$(filter ${2}.%,$(.VARIABLES)),\
	$(eval $(if ${1},${1}:) $(subst ${2}.,,${varname}) = $${${varname}}))


build.core.path=${runtime.platform.path}/cores/${${BOARD_TAG}.build.core}
build.variant.path=${runtime.platform.path}/variants/${${BOARD_TAG}.build.variant}

CORE_C_SRCS := $(shell find ${build.core.path} -name '*.c' -type f)
CORE_CPP_SRCS := $(shell find ${build.core.path} -name '*.cpp' -type f)
CORE_AS_SRCS := $(shell find ${build.core.path} -name '*.S' -type f)
CORE_OBJ_FILES := $(CORE_C_SRCS:.c=.c.o) $(CORE_CPP_SRCS:.cpp=.cpp.o) $(CORE_AS_SRCS:.S=.S.o)


C_SRCS:=$(shell find -type f -name '*.c')
CPP_SRCS:=$(shell find -type f -name '*.cpp')
INO_SRCS:=$(shell find -type f -name '*.ino')
S_SRCS:=$(shell find -type f -name '*.S')

archive_file=core.a

BUILD_DIR?=./build
build.path:=${BUILD_DIR}

OBJS:=$(patsubst %,${build.path}/%,${C_SRCS:.c=.o} ${CPP_SRCS:.cpp=.o} ${INO_SRCS:.ino=.o} ${S_SRCS:.S=.o})
CORE_OBJS := $(CORE_OBJ_FILES:${build.core.path}/%=${build.path}/core/%)
.INTERMEDIATE: ${CORE_OBJS}

compiler.cpp.flags+=-xc++
# includes+=-include Arduino.h

includes += -I${build.core.path} -I${build.variant.path}

include $(shell find ${build.path} -type f -regex ".*\.d")

$(call use_namespace,,${BOARD_TAG})
$(foreach varname,$(filter menu.cpu.%.build.board,$(.VARIABLES)),\
	$(if $(filter ${${varname}},${build.board}),\
		$(eval BOARD_SUB?=$(patsubst menu.cpu.%.build.board,%,${varname}))))
$(call use_namespace,,menu.cpu.${BOARD_SUB})

%.o: source_file=$<
%.o: object_file=$@

PREBUILD := $(patsubst recipe.hooks.%.pattern,%,$(filter recipe.hooks.prebuild.%.pattern,$(.VARIABLES)))

.PHONY: PREBUILD

prebuild.%:
	${recipe.hooks.prebuild.$*.pattern}

${build.path}/%.d: ${build.path}/%.o

${build.path}/%.c.o: %.c | $(PREBUILD)
	@mkdir -p $(@D)
	${recipe.c.o.pattern}

${build.path}/%.cpp.o: %.cpp | $(PREBUILD)
	@mkdir -p $(@D)
	${recipe.cpp.o.pattern}

${build.path}/%.ino.o: %.ino | $(PREBUILD)
	@mkdir -p $(@D)
	${recipe.cpp.o.pattern} -include Arduino.h

${build.path}/%.o: %.c | $(PREBUILD)
	@mkdir -p $(@D)
	${recipe.c.o.pattern}

${build.path}/%.o: %.cpp | $(PREBUILD)
	@mkdir -p $(@D)
	${recipe.cpp.o.pattern}

${build.path}/%.o: %.ino | $(PREBUILD)
	@mkdir -p $(@D)
	${recipe.cpp.o.pattern} -include Arduino.h

${build.path}/core/%.c.o: ${build.core.path}/%.c | $(PREBUILD)
	@mkdir -p $(@D)
	${recipe.c.o.pattern}

${build.path}/core/%.cpp.o: ${build.core.path}/%.cpp | $(PREBUILD)
	@mkdir -p $(@D)
	${recipe.cpp.o.pattern}

${build.path}/core/%.S.o: ${build.core.path}/%.S | $(PREBUILD)
	@mkdir -p $(@D)
	${recipe.S.o.pattern}

${build.path}/%.i: preprocessed_file_path=$@
${build.path}/%.i: source_file=$^
${build.path}/%.i: %.cpp
	@mkdir -p $(@D)
	${recipe.preproc.macros}

.NOTPARALLEL: ${archive_file_path}
${archive_file_path}: ${archive_file_path}(${CORE_OBJS})

${archive_file_path}(%): archive_file_path=$@
${archive_file_path}(%): object_file=$<
${archive_file_path}(%): %
	${recipe.ar.pattern}

.SECONDARY: ${build.path}/${build.project_name}.elf
%.elf: object_files=$(wordlist 2,$(words $^),$^)
%.elf: archive_file=$(<F)
%.elf: archive_file_path=$<
%.elf: ${archive_file_path} $(OBJS)
	${recipe.c.combine.pattern}


OBJCOPY = $(patsubst recipe.objcopy.%.pattern,%,$(filter recipe.objcopy.%.pattern,$(.VARIABLES)))
.DEFAULT_GOAL = objcopy
.PHONY: objcopy
objcopy: $(OBJCOPY:%=${build.path}/${build.project_name}.%)

define objcopy_rule
%.${1}: %.elf
	$${recipe.objcopy.${1}.pattern}

endef

$(foreach recipetype,$(OBJCOPY),$(eval $(call objcopy_rule,${recipetype})))



.PHONY: clean
clean:
	$(RM) -r build/*

$(call use_namespace,upload,tools.${upload.tool})
.PHONY: upload
upload: objcopy
	${upload.pattern}

$(call use_namespace,program,tools.${${PROGRAMMER}.program.tool})
$(call use_namespace,program,${PROGRAMMER})
.PHONY: program
program: objcopy
	${program.pattern}

$(call use_namespace,erase,tools.${${PROGRAMMER}.program.tool})
$(call use_namespace,erase,${PROGRAMMER})
.PHONY: erase
erase:
	${erase.pattern}

$(call use_namespace,bootloader,tools.${${PROGRAMMER}.program.tool})
$(call use_namespace,bootloader,${PROGRAMMER})
.PHONY: bootloader
bootloader:
	${bootloader.pattern}

.PHONY: monitor
monitor:
	screen ${serial.port} ${upload.speed}

.PHONY: plot
plot:
	feedgnuplot --stream --line < ${serial.port}
